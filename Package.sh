#!/bin/bash
# Install Package
curl -fsSL https://deb.nodesource.com/setup_23.x -o nodesource_setup.sh
sudo -E bash nodesource_setup.sh
sudo apt-get install -y nodejs && rm /content/nodesource_setup.sh
#
# clone
cd /content/Bot && git clone https://github.com/recitativonika/nodepay-network-bot.git
cd /content/Bot && git clone https://github.com/Zlkcyber/mesh-bot.git
cd /content/Bot && git clone https://github.com/Zlkcyber/hanafuda.git
cd /content/Bot && git clone https://github.com/Zlkcyber/functot.git
cd /content/Bot && git clone https://github.com/Zlkcyber/unichbot.git
cd /content/Bot && git clone https://github.com/airdropinsiders/plazafinance-auto-bot.git
