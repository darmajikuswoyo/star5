#!/bin/bash
#Stop All
pm2 stop hana mesh functor nodepay
#
cd /content/Bot/hanafuda && unlink token.txt
ln -s /content/Drive/hana_token.txt token.txt
pm2 start main.py --name "hana" --interpreter python3 -- -a 2
#
cd /content/Bot/mesh && unlink unique_id.txt && unlink token.txt
ln -s /content/Drive/mesh_unique_id.txt unique_id.txt
ln -s /content/Drive/mesh_token.txt token.txt
pm2 start  main.js --name "mesh"
#
cd /content/Bot/functor && unlink accounts.json
ln -s /content/Drive/functor_accounts.json accounts.json
pm2 start  main.js --name "functor"
#
cd /content/Bot/nodepay && unlink proxy.txt && unlink user.txt
ln -s /content/Drive/nodepay_Token.txt user.txt
ln -s /content/Drive/nodepay_proxy.txt proxy.txt
tee proxy.txt << EOF
http://962696bfd6f871316fdc__cr.br,ca,de,gb,us:3c6ffc8373b67c87@gw.dataimpulse.com:823
http://962696bfd6f871316fdc__cr.br,ca,de,gb,us:3c6ffc8373b67c87@gw.dataimpulse.com:823
http://962696bfd6f871316fdc__cr.br,ca,de,gb,us:3c6ffc8373b67c87@gw.dataimpulse.com:823
EOF
pm2 start main.py --name "nodepay"
#
cd /content/Bot/plaza && unlink private_keys.txt
ln -s /content/Drive/plaza_private_keys.txt private_keys.txt
pm2 start index.js --name "plaza"
#
cd /content/Bot/unich && unlink tokens.txt
ln -s /content/Drive/unich_tokens.txt tokens.txt
pm2 start main.js --name "unich"
#
#sudo rm -r /root/.pm2/logs/*