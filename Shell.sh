#!/bin/bash

wget https://github.com/rclone/rclone/releases/download/v1.53.4/rclone-v1.53.4-linux-amd64.deb
sudo dpkg -i rclone-v1.53.4-linux-amd64.deb
rm -r rclone-v1.53.4-linux-amd64.deb
sudo rclone config file
sudo pluma /home/user/.config/rclone/rclone.conf

sudo rclone mount --daemon --allow-other --vfs-cache-mode full --vfs-cache-max-age 10s --transfers 1 --cache-dir /root/Downloads/Rcache RHN:/ /root/Downloads/Videos/

wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
sudo dpkg -i google-chrome-stable_current_amd64.deb
rm -r google-chrome-stable_current_amd64.deb

google-chrome https://www.terabox.com/



