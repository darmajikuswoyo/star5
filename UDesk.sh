#!/bin/sh

sudo apt-get update && apt-get upgrade -y

sudo apt install xfce4 xfce4-goodies -y

sudo apt install xrdp -y

adduser xrdp ssl-cert
echo "xfce4-session" | tee .xsession
ufw allow 3389
systemctl restart xrdp

wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
dpkg -i google-chrome-stable_current_amd64.deb -y
apt -f install
rm -r google-chrome-stable_current_amd64.deb
